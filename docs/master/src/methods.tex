The methods which we, in this paper, wish to experiment with is divided into two parts, namely optimizing algorithms for training and regularization techniques during training.

\subsection{Optimizing Training Algorithms}
Often we want to optimize the training scheme of a neural net by focusing on how we update our parameters. This section will give a brief introduction to some of these schemes.
%\todo[inline]{Add reference to paper: https://arxiv.org/pdf/1609.04747v1.pdf} 
\ \\
\subsubsection{Stochastic Gradient Descent}
In gradient descent we want minimize the loss function $ \mathfrak{L}\left( \theta \right)$ by following the opposite direction of the gradient of the multidimensional \textit{surface} extended by the parameters, $\nabla_{\theta} \mathfrak{L}\left( \theta \right)$, thus finding a local minima. Gradient descent comes in various flavours however a popular approach is the \gls{sgd} based on mini-batch. \gls{sgd} updates parameters, $\theta$, with a step size (learning rate), $\eta$,  for a small random portion of the training sample, also known as \textit{batch}, given the inputs $x_i$ and the corresponding labels $y_i$. (see Eq. \ref{eq:batch_sgd})
	\begin{equation}
		\label{eq:batch_sgd}
		\theta = \theta - \eta \cdot \nabla_{\theta} \mathfrak{L}(\theta; x_{i:i+n}, y_{i:i+n})
	\end{equation}

This method will fluctuate more than the gradient descent and should therefore be accompanied by a carefully selected learning rate. On the upside the method enables us to jump to other local minima which might lead to better results.
\ \\
\subsubsection{Momentum Optimization}
The parameter surface might be steeper in some dimensions than other which can cause \gls{sgd} to oscillate and ultimately slow down progress. The momentum optimizer \cite{qian1999momentum} reduces oscillations by adding a fraction $\gamma$ of the update vector from the previous step to the current one (see Eq. \ref{eq:momentum_opt}). 
					
	\begin{subequations}
		\label{eq:momentum_opt}
		\begin{align}
			v_t & = \gamma v_{t-1} + \eta \nabla_{\theta} \mathfrak{L}\left( \theta \right)\\
			\theta & = \theta- v_t 
		\end{align}
	\end{subequations}
	
This way consistent gradients will be more dominant than small steep areas on the surface. The moment optimizer dampens oscillations and accelerate convergence.
\ \\
\subsubsection{Adaptive Moment Estimation (Adam) Optimization}
Carefully choosing a proper learning rate is not a trivial task. A too low learning rate can require the network to train for days however choosing too high can yield overshooting and oscillating as a consequence. \gls{adam} tries to deal with this by automatically adapting the learning rate so that for each parameter at each step there is a corresponding learning rate. The idea is to divide the learning rate by some factor derived by the gradient. In its simplest form ( Adagrad \cite{duchi2011adaptive}  Eq. \ref{eq:adagrad}) we divide by the sum of all previous squared gradients, $G_{t,ii}$, yielding a lower learning rate in steep areas. The term $\epsilon$ is a small number preventing us from dividing by zero.

	\begin{equation}
	\label{eq:adagrad}
	\theta_{t+1,i} = \theta_{t,i} - \frac{\eta}{\sqrt{G_{t,ii}+\epsilon}} \cdot \nabla_{\theta} \mathfrak{L}\left( \theta_i \right)
	\end{equation}
	
In its nature the learning rate will be continuously decreasing eventually preventing the network to learn anything new. \gls{adam} \cite{kingma2014adam} is less crude as it first of all uses an exponentially decaying average of the previous squared gradients, $v_t$, as a replacement for $G_t$. It furthermore uses the same averaging scheme on the previous non-squared gradients, $m_t$, as a replacement for the current gradient. The vector $m_t$ is the mean or first moment while $v_t$ is the second moment or uncentred variance of the gradient.  

	\begin{subequations}
		\begin{align}
			m_t &= \beta_1 m_{t-1} + \left( 1 - \beta_1 \right) g_t \\
			v_t &= \beta_2 v_{t-1} + \left( 1 - \beta_2 \right) g_t^2\\
			\hat{m}_t &= \frac{m_t}{1-\beta_{1}^t}\\
			\hat{v}_t &= \frac{m_t}{1-\beta_{2}^t}
		\end{align}
	\end{subequations}

Here $g_t$ denotes the gradient $( \nabla_{\theta} \mathfrak{L}\left( \theta \right) )$ and the $g_t^2$ is the element-wise squared gradient. $\beta$ denotes the decay rate ie. the power of the most resent gradient. $\hat{m}_t$ and $\hat{v}_t$ are bias corrected variants. The Adam parameter update rule can be seen in Eq. \ref{eq:adam}.

	\begin{equation}
	\label{eq:adam}
	\theta_{t+1} = \theta_{t} - \frac{\eta}{\sqrt{\hat{v}_{t}}+\epsilon} \hat{m}_t
	\end{equation} 

\subsection{Regularization Techniques}
A major problem when training \glspl{ann} is called overfitting, which is used to describe how well an \gls{ann} generalizes to new data. The generalization ability of the network depends on a balance between the information available in the training data and the complexity of the network, where the complexity of a network is often denoted by the number of free parameters, e.g. weights. If the \gls{ann} is overfitting, then the network error of the \gls{ann} is driven to a very small value during training and when new data is presented suddenly the network error becomes larger. This is means that the \gls{ann} has learned to memorize the training data, but not learned to generalize well to unseen data, which is highly undesirable. This bad generalization happens when the complexity of the training data does not match the one of the network. Luckily there exists many ways of preventing the \gls{ann} to overfit to the training data.
\ \\
\subsubsection{Weight Decay}
The weight decay, described by Krogh \etal in \cite{moody1995simple}, is a method which attempts to limit the growth of the weights in a network. The goal is that the weights should be as small as possible, and is achieved by introducing a penalty term to the cost function \eqref{eq:wd_penalty_term_cost_function},
\begin{equation}
 C(\bm{w}) = C_0(\bm{w})+\frac{1}{2}\lambda\sum\limits_i w_i^2
 \label{eq:wd_penalty_term_cost_function}
\end{equation}
where $C_0$ is the cost (error) measure, e.g. sum of squared errors and $\lambda$ denotes the strength of the penalty.
\ \\
\subsubsection{Dropout}
Dropout, by Hinton and Srivastava \etal \cite{hinton2012improving,krizhevsky2012imagenet,srivastava2014dropout}, is another simple way of preventing an \gls{ann} to overfit the training data. It works by, at random, canceling out neurons and its connections during training, where the remaining neurons then forms a thinned network, as shown in Fig. \ref{fig:dropout}. In this way, an \gls{ann} consisting of $n$ neurons is seen as a collection of $2^n$ possible thinned networks. Each of these networks gets trained very rarely when applying dropout, if at all. This means that it is not feasible to explicitly average the predictions from the exponentially many networks at test time. Instead each neuron's weight are multiplied with its probability $p$ of being canceled out. The scaling of the weights enables the thinned networks to be combined into one complete model. It is believed that this regularization technique prevents the neurons from co--adapting, where the intuition originates from the extensive testing \cite{srivastava2014dropout}.

\noindent The output from an \gls{ann} with dropout can be expressed as
\begin{equation}
 \bm{y} = \bm{m} \star f(W\bm{x})
 \label{eq:dropout_y}
\end{equation}
where $\star$ is the element wise product, $\bm{m}$ is a binary mask vector, where each element $m_i$ is drawn independently from a Bernoulli distribution with probability $p$, $m_i \iid \mathrm{Bernoulli}(p)$. It is stated by \cite{wan2013dropconnect} that $f(0)=0$ holds for the $\tanh$, centered sigmoid and relu activation functions, why \eqref{eq:dropout_y} can be expressed as
\begin{equation}
 \bm{y} = f(\bm{m}\star W\bm{x})
 \label{eq:dropout_y_f0}
\end{equation}



\begin{figure}
\centering
\input{img/tikz_dropout}
\caption[Illustration of a forward connected \gls{ann} with dropout applied]{The figure illustrates a forward connected \gls{ann} with dropout applied.}\label{fig:dropout}
\end{figure}
