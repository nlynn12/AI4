data <- read.csv("feedforward_ann_10000e_train.csv")
data2 <- read.csv("feedforward_ann_10000e_test.csv")

#print(data2[,3])

pts <- pretty(data[,2] / 1000000)

setEPS()
postscript("t.eps")
cl = rainbow(20)
plot(data[,2]/1000000, data[,3], col=cl[2], type="l", xlab="Steps", ylab="Accuracy", main="Accuracy for Simple Feedforward ANN on MNIST", axes=FALSE)
lines(data2[,2]/1000000,data2[,3],col="blue")
legend(0,0, c("Step","Value"), lty=c(1,1), lwd=c(2.5,2.5),col=c("blue","red"))
grid (NULL,NULL, lty = 6, col="lightgray")
axis(side = 2, at = c(seq(from=0.0,to=1.05,by=0.05)))
axis(side = 1, at = pts, labels=paste(pts, "M", sep=""), las = 1)#c(seq(from=-500000,to=5000000,by=500000)))
box()
q = dev.off()


.f = function(){

}
