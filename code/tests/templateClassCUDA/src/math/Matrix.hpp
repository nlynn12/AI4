/*
 * Copyright 2016 Nicolai Anton Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef _MATRIX_HPP
#define _MATRIX_HPP

#include <iostream>
#include <stdio.h>

#include <thrust/host_vector.h>

#include "MatrixKernels.hpp"


template<typename T>
class Matrix{
  private:
    int _rows;
    int _cols;
    thrust::host_vector<T> _data;
  public:
    Matrix(int rows, int cols);
    Matrix<T>& operator=(const Matrix<T>& other);
    Matrix<T> operator*(const Matrix<T>& other);
    T& operator()(int idx) {return _data[idx];}
    int rows() const {return _rows;}
    int cols() const {return _cols;}
    void print();
};

template<typename T> Matrix<T>::Matrix(int rows, int cols){
  _rows = rows;
  _cols = cols;
  
  // Allocate memory for host_vector
  _data.resize(rows*cols);
  for(size_t i = 0; i < _data.size(); i++){
    _data[i] = i;
  }
  std::cout << "Size: " << _data.size() << " (" << rows << "*"<< cols << " = " << rows*cols << ")" << std::endl;
}

template<typename T> Matrix<T>& Matrix<T>::operator=(const Matrix<T>& other){
  // Hidden Kernel Call
  initializeMatrix<T>(_rows,_cols);
    
  Matrix<T> lol(2,2);
  
  return lol;
}

template<typename T> Matrix<T> Matrix<T>::operator*(const Matrix<T>& other){
  Matrix<T> res(this->rows(), other.cols());
  cublas_mmmult/*<T>*/(this->_data, other._data, res._data, this->rows(),this->cols(), other.cols());
  res.print();
  return res;
}

template<typename T> void Matrix<T>::print(){
  for(size_t i = 0; i < this->rows(); ++i){
    for(size_t j = 0; j < this->cols(); ++j){
      std::cout << this->operator()(j * this->rows() + i) << " ";
    }
    std::cout << std::endl;
  }
  std::cout << std::endl;
}

#endif