#include "MatrixKernels.hpp"

template<typename T>
__global__
void initializeMatrixKernel(T *x)
{
  //printf("%i\n",threadIdx.x);
}

template<typename T>
int initializeMatrix(int rows, int cols){
  T *x, *d_x;
  int N = rows*cols;
  x = (T*)malloc(N*sizeof(T));

  cudaMalloc(&d_x, N*sizeof(T)); 
  cudaMemcpy(d_x, x, N*sizeof(T), cudaMemcpyHostToDevice);

  // Initialize matrix
  initializeMatrixKernel<T><<<1, N>>>(d_x);

  cudaMemcpy(x, d_x, N*sizeof(T), cudaMemcpyDeviceToHost);
  
  return 0;
}

// Multiply the arrays A and B on GPU and save the result in C
// C(m,n) = A(m,k) * B(k,n)
//template<typename T>
void cublas_mmmult(thrust::host_vector<float> th,thrust::host_vector<float> ot, thrust::host_vector<float> r, const int m, const int k, const int n){
  std::cout << th.size() << std::endl;
  std::cout << ot.size() << std::endl;
  std::cout << r.size() << std::endl;

  std::cout << "C(m,n) = A(m,k) * B(k,n)\n" << "(" << m << "," << n << ") = " << " (" << m << "," << k << ") * " "(" << k << "," << n << ")" << std::endl; 
  
  float* A=thrust::raw_pointer_cast(&th[0]);
  float* B=thrust::raw_pointer_cast(&ot[0]);
  float* C=thrust::raw_pointer_cast(&r[0]);
  
  float * d_a,*d_b,*d_c;
  cudaMalloc(&d_a,m*k*sizeof(float));
  cudaMalloc(&d_b,k*n*sizeof(float));
  cudaMalloc(&d_c,m*n*sizeof(float));
  
  int lda=m,ldb=k,ldc=n;
  const float alf = 1;
  const float bet = 0;
  const float *alpha = &alf;
  const float *beta = &bet;
 
  // Create a handle for CUBLAS
  cublasHandle_t handle;
  cublasCreate(&handle);
 
  // Do the actual multiplication
  cublasSgemm(handle, CUBLAS_OP_N, CUBLAS_OP_N, m, n, k, alpha, A, lda, B, ldb, beta, C, ldc);
  
  for(int i = 0; i < r.size(); i++)
    std::cout << C[i] << " ";
  std::cout << "\n";
  
  // Result should be:
  // 10 28 46 64
  // 13 40 67 94
  
  // Destroy the handle
  cublasDestroy(handle);
};



// HACK from http://www.codeproject.com/Articles/48575/How-to-define-a-template-class-in-a-h-file-and-imp

void tmp(){
  initializeMatrix<float>(0,0);
  //cublas_mmmult<float>(thrust::host_vector<float>(),thrust::host_vector<float>(), thrust::host_vector<float>(),0,0,0);
}