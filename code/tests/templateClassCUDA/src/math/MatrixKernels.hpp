#ifndef _MATRIX_KERNELS_HPP
#define _MATRIX_KERNELS_HPP

#include <cuda.h>
#include <cuda_runtime.h>

#include <iostream>
#include <stdio.h>

#include <thrust/host_vector.h>

#include <cublas_v2.h>

// HACK from http://www.codeproject.com/Articles/48575/How-to-define-a-template-class-in-a-h-file-and-imp

template<typename T>
int initializeMatrix(int rows, int cols);

//template<typename T>
void cublas_mmmult(thrust::host_vector<float> th,thrust::host_vector<float> ot, thrust::host_vector<float> r, const int m, const int k, const int n);

//#include "MatrixKernels.cu"


#endif