cmake_minimum_required(VERSION 2.8.8)
project(TemplateClassCUDA)

FIND_PACKAGE(CUDA REQUIRED)

cuda_add_executable(
    ${PROJECT_NAME}
      src/main.cpp
      src/math/Matrix.hpp
      src/math/MatrixKernels.hpp
      src/math/MatrixKernels.cu
)

list(APPEND CUDA_NVCC_FLAGS "-arch=sm_35;-std=c++11;-O2;-DVERBOSE")
list(APPEND CMAKE_CXX_FLAGS "-std=c++11 ${CMAKE_CXX_FLAGS} -g -ftest-coverage -fprofile-arcs")


target_link_libraries(
    ${PROJECT_NAME}
      ${CUDA_LIBRARIES}
      ${CUDA_CUBLAS_LIBRARIES}
)

set(CMAKE_BUILD_TYPE Release)
