# Copyright 2016 Nicolai Anton Lynnerup. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from __future__ import absolute_import, division, print_function

import tensorflow as tf

# Read MNIST Dataset (http://yann.lecun.com/exdb/mnist/)
from tensorflow.examples.tutorials.mnist import input_data
mnist = input_data.read_data_sets("/tmp/data/", one_hot=True)

import numpy as np
import matplotlib.pyplot as plt

import sys

learning_rate = 0.001
training_epochs = 2500
batch_size = 128
display_step = 1
logs_path = "/home/nlynn/Documents/AI4/log/ann/adam/no_reg"

n_inputs = 784
n_hidden_1 = 512 #256
n_hidden_2 = 256 #128
n_classes = 10

x = tf.placeholder(tf.float32, [None, n_inputs])
y = tf.placeholder(tf.float32, [None, n_classes])
W = {
    'h1': tf.Variable(tf.random_normal([n_inputs, n_hidden_1])),
    'h2': tf.Variable(tf.random_normal([n_hidden_1, n_hidden_2])),
    'o': tf.Variable(tf.random_normal([n_hidden_2, n_classes]))
}
b = {
    'b1': tf.Variable(tf.random_normal([n_hidden_1])),
    'b2': tf.Variable(tf.random_normal([n_hidden_2])),
    'o': tf.Variable(tf.random_normal([n_classes]))
}

#keep_prob = tf.placeholder(tf.float32)

def mlp(x, W, b):
    l1 = tf.nn.sigmoid(tf.add(tf.matmul(x, W['h1']), b['b1']))
    #l1 = tf.nn.dropout(l1,0.5)
    l2 = tf.nn.sigmoid(tf.add(tf.matmul(l1, W['h2']), b['b2']))
    #l2 = tf.nn.dropout(l2,0.5)
    return tf.add(tf.matmul(l2, W['o']), b['o'])

# Create model
pred = mlp(x, W, b)
# Define cost function
cost = tf.reduce_mean(tf.nn.softmax_cross_entropy_with_logits(pred, y))

##Add Weight Decay
##lambda = tf.constant(0.9)

#l2_loss = tf.add_n([tf.nn.l2_loss(v) for v in tf.trainable_variables()])
#cost = cost + 0.00008 * l2_loss

# Define optimizer function
#optimizer = tf.train.GradientDescentOptimizer(learning_rate).minimize(cost)
#optimizer = tf.train.MomentumOptimizer(learning_rate,0.9).minimize(cost)
optimizer = tf.train.AdamOptimizer(learning_rate).minimize(cost)

# Tensorboard
with tf.name_scope('Accuracy'):
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, tf.float32))

tf.summary.scalar("accuracy", accuracy)
merged_summary_op = tf.summary.merge_all()

with tf.Session() as sess:
    sess.run(tf.global_variables_initializer())
    sess.run(tf.local_variables_initializer())

    train_writer = tf.summary.FileWriter(logs_path + '/train', sess.graph)
    test_writer = tf.summary.FileWriter(logs_path + '/test')

    total_batch = int(mnist.train.num_examples / batch_size)

    for epoch in range(training_epochs):
        for i in range(total_batch):
            batch_x, batch_y = mnist.train.next_batch(batch_size)

            # Run optimizer operation
            _, c, summary = sess.run([optimizer, cost, merged_summary_op], feed_dict={
                                     x: batch_x, y: batch_y
                                     #, keep_prob: 0.5
                                     })
            train_writer.add_summary(summary, epoch * total_batch + i)
            #train_writer.flush()
            #print("Writing train validation summary at step: " + str(i))

        # Compute accuracy on test part of dataset
        print("Writing test validation summary at epoch: " + str(epoch))
        summary, acc = sess.run([merged_summary_op, accuracy], feed_dict={
                                 x: mnist.test.images, y: mnist.test.labels
                                 #, keep_prob: 1.
                                 })
        test_writer.add_summary(summary, (epoch+1) * total_batch)

        # Display log
        #if epoch % display_step == 0:
        #    print("Epoch:", '%04d' % (epoch))

    train_writer.close()
    test_writer.close()
    print("Optimization Finished!")

    # Test model
    correct_prediction = tf.equal(tf.argmax(pred, 1), tf.argmax(y, 1))
    # Calculate accuracy
    accuracy = tf.reduce_mean(tf.cast(correct_prediction, "float"))
    print("Accuracy:", accuracy.eval({x: mnist.test.images, y: mnist.test.labels}))
