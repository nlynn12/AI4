# Dependencies

* The projects in this folder uses [FANN](http://leenissen.dk/fann/wp/) as its framework, make sure its installed.

### Installing FANN

* Change directory to where you want the library to be placed. Here it's placed in `~/Programs/`

```shell
cd ~/Programs/
wget https://github.com/libfann/fann/archive/master.zip
unzip master.zip
cd fann-master/
```

* Compile the library

```shell
cmake .
sudo make install
sudo ldconfig
```

* Verify that it is installed correctly by building the examples:

```shell
cd examples/
make runtest
```


#### Sources:
* [Installing FANN](http://leenissen.dk/fann/wp/help/installing-fann/)
* [Github/libfann](https://github.com/libfann/fann)
