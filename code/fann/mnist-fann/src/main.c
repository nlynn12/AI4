#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "fann.h"

struct fann* create_net(){
    const float learning_rate = 1.e-3;
    const unsigned int num_input = 784;
    const unsigned int num_output = 10;
    const unsigned int num_layers = 3;
    const unsigned int num_neurons_hidden = 300;
    //const float desired_error = 0.0001;
    //const unsigned int max_iterations = 500000;
    //const unsigned int iterations_between_reports = 1;

    struct fann *ann = fann_create_standard( num_layers,
                                             num_input,
                                             num_neurons_hidden,
                                             num_output);

    fann_set_activation_function_hidden(ann, FANN_SIGMOID);
    fann_set_activation_function_output(ann, FANN_SIGMOID);
    fann_set_learning_rate(ann, learning_rate);

    return ann;
}

int argmax(fann_type* y,int nelements){
    int i;
    fann_type maxval = 0.0;
    int maxarg = -1;
    for(i = 0; i < nelements; ++i){
        //printf("%f ",y[i]);
        if(y[i] > maxval){
            maxval = y[i];
            maxarg = i;
        }
    }
    //printf("\n");
    return maxarg;
}

double get_classification_error(struct fann* ann, struct fann_train_data* test_data){
    double err = 0;
    int i;
    fann_type* out;
    int noutputs = test_data->num_output;
    for(i = 0; i < test_data->num_data; ++i){
        fann_type* x = test_data->input[i];
        fann_type* y = test_data->output[i];
        out = fann_run(ann, x);
        int net_class = argmax(out, noutputs);
        int true_class = argmax(y, noutputs);
        if(net_class != true_class){
            err += 1.0;
            /*int j;
            for(j = 0; j < noutputs; ++j){
                printf("%f ",out[j]);
            }
            printf("\n");
            for(j = 0; j < noutputs; ++j){
                printf("%f ",y[j]);
            }
            printf("\n\n");*/
        }
    }
    err /= (double)test_data->num_data;
    return err;
}

void train_net(struct fann* ann, struct fann_train_data* trainData, struct fann_train_data* testData, int numIterations){
    int i;
    int j;
    char timestr[500];
    time_t t;
    struct tm *tmp;

    printf( "trainNet: Beginning training\n" );

    for(i = 0; i < numIterations; ++i)  {
        for(j = 0; j < trainData->num_data; ++j)  {
            fann_type* x = trainData->input[j];
            fann_type* y = trainData->output[j];
            fann_train(ann, x, y);
        }
        double err = get_classification_error(ann, testData);
        t = time(NULL);
        tmp = localtime(&t);

        if(strftime(timestr, sizeof(timestr), "%Y-%m-%d %H:%M:%S", tmp) == 0) {
            fprintf(stderr, "strftime returned 0");
            exit(EXIT_FAILURE);
        }
        printf("%s epoch = %6d, classification error = %8.4f%%\n", timestr, i, 100.0 * err);
    }
}

int main(int argc, char* argv[]){
    char* train_file = "../src/train.fann";
    char* test_file = "../src/test.fann";

    srand(1);

    printf("Loading training data\n");
    struct fann_train_data* train_data = fann_read_train_from_file(train_file);

    printf("Loading test data\n");
    struct fann_train_data* test_data = fann_read_train_from_file(test_file);

    printf("Creating net\n");
    struct fann* ann = create_net();

    printf("Training net\n");
    train_net(ann, train_data, test_data, 500);
    
    printf("Saving net\n");
    fann_save(ann, "fann.net");

    fann_destroy(ann);

    return 0;
}
