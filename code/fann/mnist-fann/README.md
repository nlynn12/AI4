# About

This project is made with inspiration from [https://github.com/tgflynn/mnist-fann](https://github.com/tgflynn/mnist-fann)

# Configuring this project

### Automatically
   * Run the bash script

```shell
chmod +x configure.sh
./configure.sh
```


### Manually
   * Create build folder

```shell
mkdir build
cd build/
cmake ../
make
./mnist-fann
```



   * Download MNIST:

```shell
mkdir mnist
cd mnist/
wget http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz
```

   * Unpack the dataset

```shell
gunzip train-images-idx3-ubyte.gz
gunzip train-labels-idx1-ubyte.gz
gunzip t10k-images-idx3-ubyte.gz
gunzip t10k-labels-idx1-ubyte.gz
```

   * Format the dataset

```shell
cd src/
./idxToCsv.py -i ../mnist/train-images.idx3-ubyte -l ../mnist/train-labels.idx1-ubyte -o train.fann -f FANN
./idxToCsv.py -i ../mnist/t10k-images.idx3-ubyte -l ../mnist/t10k-labels.idx1-ubyte -o test.fann -f FANN
```
