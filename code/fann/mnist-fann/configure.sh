#!/bin/bash

# Build project
mkdir build
cd build/
cmake ../
make

# Download MNIST
cd ../
mkdir mnist
cd mnist/
wget http://yann.lecun.com/exdb/mnist/train-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/train-labels-idx1-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-images-idx3-ubyte.gz
wget http://yann.lecun.com/exdb/mnist/t10k-labels-idx1-ubyte.gz

gunzip train-images-idx3-ubyte.gz
gunzip train-labels-idx1-ubyte.gz
gunzip t10k-images-idx3-ubyte.gz
gunzip t10k-labels-idx1-ubyte.gz

# Reformat MNIST so it fits FANN
cd ../src/
./idxToCsv.py -i ../mnist/train-images-idx3-ubyte -l ../mnist/train-labels-idx1-ubyte -o train.fann -f FANN
./idxToCsv.py -i ../mnist/t10k-images-idx3-ubyte -l ../mnist/t10k-labels-idx1-ubyte -o test.fann -f FANN
