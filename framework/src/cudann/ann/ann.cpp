/*
 * Copyright 2016 Nicolai Anton Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#include "ann.hpp"
#include <boost/concept_check.hpp>

using namespace cudann::ann;
using namespace cudann::common;

ANN::ANN() : _layout(), _activations(), _weights(), _biases(){
   
}

ANN::~ANN() {

}

void ANN::create(const std::string network_file) {
   std::cout << "Constructing ANN" << std::endl;
   std::cout << "   Reading from: " << network_file << std::endl;
   
   // Read network file
   
   
   
   // Load weights
   std::cout << "   Loading pre-trained network" << std::endl;
   
   // From FANN on XOR (2,2,1)
   // (connected_to_neuron, weight)=(0, -2.32955598831176757812e+00) (1, -2.36318492889404296875e+00) (2, 1.87986934185028076172e+00) (0, 4.64037179946899414062e+00) (1, 4.65584135055541992188e+00) (2, 3.65738391876220703125e+00) (3, 5.31295728683471679688e+00) (4, 4.88048744201660156250e+00) (5, -4.14780998229980468750e+00) 
   _layout = std::vector<int>{2,2,1};
   _biases.clear();
   _weights.clear();
   
   for(size_t i = 1; i < _layout.size(); i++){
      _biases.push_back(Matrix<float>(1,_layout[i],i));
      _weights.push_back(Matrix<float>(_layout[i-1],_layout[i],i));
   }
   
   _weights[0].set(0,0,-2.32955598831176757812e+00);
   _weights[0].set(0,1,4.64037179946899414062e+00);
   _weights[0].set(1,0,-2.36318492889404296875e+00);
   _weights[0].set(1,1,4.65584135055541992188e+00);
   
   _biases[0].set(0,0,1.87986934185028076172e+00);
   _biases[0].set(0,1,3.65738391876220703125e+00);
   
   _weights[1].set(0,0,5.31295728683471679688e+00);
   _weights[1].set(0,1,4.88048744201660156250e+00);
   
   _biases[0].set(0,0,-4.14780998229980468750e+00);
   /*
    * 0.920381 1.19151 0.61163 2.29805 
    * 0.972805 1.23359 0.46459 2.26624 
    * -0.141264 -1.67153 -0.416143 -0.766453 
    * -0.0252511 
    * -1.94468 
    * -0.752815 
    * 2.31795 
    * -0.617132 
    */
   /*
   _weights[0].set(0,0,0.920381);
   _weights[0].set(0,1,1.19151);
   _weights[0].set(0,2,0.61163);
   _weights[0].set(0,3,2.29805);
   
   _weights[0].set(1,0,0.972805);
   _weights[0].set(1,1,1.23359);
   _weights[0].set(1,2,0.46459);
   _weights[0].set(1,3,2.26624);
   _weights[0].print();
   _biases[0].set(0,0,-0.141264);
   _biases[0].set(0,1,-1.67153);
   _biases[0].set(0,2,-0.416143);
   _biases[0].set(0,3,-0.766453);
   
   _weights[1].set(0,0,-0.0252511);
   _weights[1].set(0,1,-1.94468);
   _weights[1].set(0,2,-0.752815);
   _weights[1].set(0,3,2.31795);
   
   _biases[1].set(0,0,-0.617132);*/
   
   //for(size_t i = 0; i < _weights.size(); i++){
   //   _weights[i].print();
   //}
   
   //for(size_t i = 0; i < _biases.size(); i++){
   //   _biases[i].print();
   //}
   
   // if(success)
   std::cout << "   Done!" << std::endl << std::endl;
}

void ANN::create(std::vector<int> *layout){
   std::cout << "Constructing ANN" << std::endl;
   std::cout << "   Of dimension:";
   for(auto dim : *layout) std::cout << " " << dim;
   std::cout << std::endl << std::endl;
   
   _layout = *layout;
   
   // Initialize ANN
   initialize();
}

/**
 * @todo Add bounded random number generator
 */
void ANN::initialize(){
   _biases.clear();
   _weights.clear();
   
   for(size_t i = 1; i < _layout.size(); i++){
      _biases.push_back(Matrix<float>(1,_layout[i],i)); // rnd_number
      _weights.push_back(Matrix<float>(_layout[i-1],_layout[i],i)); // , rnd_number!
   }
   
   for(size_t i = 0; i < _weights.size(); i++){
      _weights[i].print();
   }
}

void ANN::set_activation_function(std::function<float(float)> activation){
   _activation_function = activation;
}

void ANN::set_activation_function(CUDANN_ANN_ACTIVATION_FUNCTION a_func){
   switch(a_func){
      case tanh:
	 _activation_function = ([](float activation){return (2.0/(1.0+std::exp(-2.0*activation))-1.0);});
	 break;
      defalt:
	 std::cout << "Activation Function Unknown Error!" << std::endl;
	 break;
   }
}

void ANN::feedforward(const Matrix<float> &input){
   _activations.clear();
   
   _activations.push_back(input);
   
   size_t batch_size = (size_t)input.rows();
   
   for(size_t i = 0; i < _weights.size(); i++){
      // Get current weight matrix
      auto &w = _weights[i];
      
      // Compute input * weight
      Matrix<float> input_weight = _activations.back()*w;

      // Add bias to input--weight product
      Matrix<float> input_weight_bias = input_weight + _biases[i].replicate(batch_size);
      input_weight_bias.print();
      
      // Apply activation function to input--weight (+ bias) product
      Matrix<float> activation = input_weight_bias.unary_expr(_activation_function);
      activation.print();
      _activations.push_back(activation);
   }
   
   std::cout << "Result of feedforward" << std::endl;
   _activations.back().print();
}