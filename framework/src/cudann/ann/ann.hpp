/*
 * Copyright 2016 Nicolai Anton Lynnerup <nlynn12@student.sdu.dk>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

#ifndef _CUDANN_ANN_ANN_HPP
#define _CUDANN_ANN_ANN_HPP

#include <iostream>
#include <string>
#include <vector>
#include <functional>

#ifdef USE_CUDA
#include <cublas_v2.h>
#endif

#include "../common/matrix.hpp"
#include "../common/ptr.hpp"
#include "../config.hpp"


namespace cudann {
   namespace ann {
      class ANN {
	 private:
	    std::vector<int> _layout;
	    std::vector<cudann::common::Matrix<float>> _activations;
	    std::vector<cudann::common::Matrix<float>> _weights;
	    std::vector<cudann::common::Matrix<float>> _biases;
	    std::function<float(float)> _activation_function = ([](float activation){return (2.0/(1.0+std::exp(-2.0*activation))-1.0);}); //tanh
	    void create_neural_network(std::vector<int>);
	    void initialize();
	 public:
	    enum CUDANN_ANN_ACTIVATION_FUNCTION{
	       tanh
	    };
	    typedef cudann::common::Ptr<ANN> Ptr;
	    ANN();
	    ~ANN();
	    void create(const std::string);
	    void create(std::vector<int> *);
	    void set_activation_function(std::function<float(float)>);
	    void set_activation_function(CUDANN_ANN_ACTIVATION_FUNCTION);
	    void feedforward(const cudann::common::Matrix<float>&);
      };
   }
}

void export_ann();

#endif // _CUDANN_ANN_ANN_HPP
