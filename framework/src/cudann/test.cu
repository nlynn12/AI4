#include "test.hpp"
#include <thrust/device_vector.h>
#include <thrust/reduce.h>
#include <thrust/sequence.h>

template<typename T> struct Fun {
    __device__ T operator() ( T t1, T t2 ) {
        auto result = t1+t2;
        return result;
    }
};

/**
 * @brief 
 * @return
 */ 
int run(int n) {
    const int N = n;
    thrust::device_vector<int> vec ( N );
    thrust::sequence ( vec.begin(),vec.end() );
    //for(auto it : vec.size()) std::cout << it << std::endl;
    auto op = Fun<int>();
    return thrust::reduce ( vec.begin(),vec.end(),0,op );
}
