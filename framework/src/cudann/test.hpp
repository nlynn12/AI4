#ifndef TEST_H
#define TEST_H

#include "config.hpp"

#if USE_CUDA == 1
int run(int);
#endif

#endif
