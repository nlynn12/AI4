/**
 * @file cudann.cpp
 * @brief This file contains the version handling of the framework
 * 
 * @todo Test classes (both cpp and py)
 * @todo Test namespaces (both cpp and py)
 */

#include "cudann.hpp"

/**
 * @brief This function returns the version of the framework
 * 
 * <b>MWE in C++</b>
 * @code{.cpp}
 * #include <iostream>
 * #include "cudann.h"
 * 
 * int main ( int argc, char *argv[] ) {
 *     std::cout << version() << std::endl;
 *     return 0;
 * }
 * @endcode
 * <b>MWE in Python</b>
 * @code{.py}
 * #!/usr/bin/python
 * 
 * import sys
 * sys.path.append("/usr/local/lib/")
 * import libcudann
 * 
 * print libcudann.version()
 * @endcode
 * 
 * @returns Returns a C++ string with the CUDANN Version e.g. "0.1.0"
 */
std::string version(){
        return CUDANN_VERSION_STRING;
}