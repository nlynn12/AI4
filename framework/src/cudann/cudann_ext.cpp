#include "cudann.hpp"
#include "ann/ann.hpp"

#include <boost/python.hpp>

using namespace boost::python;
 
BOOST_PYTHON_MODULE(libcudann)
{
   def( "version", version );
   
   export_ann();
}
