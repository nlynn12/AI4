/*
 * Copyright 2016 Nicolai Anton Lynnerup <nlynn12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef _CUDANN_MATH_MATRIX_HPP
#define _CUDANN_MATH_MATRIX_HPP

#include <iostream>
#include <cstdio>
#include <functional>

#ifdef USE_CUDA
#include <cuda.h>
#include <cuda_runtime.h>
#endif

namespace cudann {
   namespace math {
      template <typename T, typename implementation >
      class Matrix{
	 private:
	    int _rows;
	    int _cols;
	    T **_data;
	 public:
	    Matrix(int rows, int cols, T val);
	    //Matrix(const Matrix<T>& copy);
	    ~Matrix();
	    /*Matrix<T>& Matrix<T>::operator=(const Matrix<T>& other){
	       return implementation.operator=(this=other);
	    }*/
	    void sum(){
	       implementation<T>.sum();
	    }
	    /*Matrix<T> operator-(const Matrix<T>& other);
	    Matrix<T> operator+(const Matrix<T>& other);
	    Matrix<T> operator*(const Matrix<T>& other);	// Multiply w/ other Matrix
	    Matrix<T> operator*(const T& scalar);		// Multiply w/ scalar
	    T& operator()(const int row, const int col) const;
	    bool operator==(const Matrix& other);
	    Matrix<T> transpose();
	    Matrix<T> identity();
	    Matrix<T> replicate(int cols);
	    Matrix<T> unary_expr(std::function<T(T)> activation);*/
	    void set(int row,int col,T value);
	    int rows() const {return _rows;}
	    int cols() const {return _cols;}
	    void print();
      };
      
      /*template<class T> Matrix<T>::Matrix(int rows, int cols, T val = 0){
	 _rows = rows;
	 _cols = cols;
	 _data = new T*[_rows];
	 
	 for(size_t i = 0; i < _rows; i++){
	    _data[i] = new T [_cols];
	 }
	 
	 for(size_t i = 0; i < _rows; i++){
	    for(size_t j = 0; j < _cols; j++){
	       _data[i][j] = val;
	    }
	 }
      }
      
      template<class T> Matrix<T>::Matrix(const Matrix<T>& copy) : _rows(copy.rows()), _cols(copy.cols()){
	 _data = new T*[_rows];
	 for(size_t i = 0; i < _rows; i++){
	    _data[i] = new T [_cols];
	    for(size_t j = 0; j < _cols; j++){
	       _data[i][j] = copy(i,j);
	    }
	 }
      }

      template<class T> Matrix<T>::~Matrix(){
	 for(int i = 0; i < _rows; i++) {
	    delete [] _data[i];
	 }
	 delete [] _data;
      }*/
      
      
      /*
      template<class T> Matrix<T> Matrix<T>::operator-(const Matrix<T>& other){
	 Matrix<T> res(this->rows(), this->cols());

	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < other.cols();j++){
	       T val = this->operator()(i,j) - other(i,j);
	       res.set(i,j,val);
	    }
	 }
	 return res;
      }
      
      template<class T> Matrix<T> Matrix<T>::operator+(const Matrix<T>& other){
	 Matrix<T> res(this->rows(), this->cols());

	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < other.cols();j++){
	       T val = this->operator()(i,j) + other(i,j);
	       res.set(i,j,val);
	    }
	 }
	 return res;
      }
      
      template<class T> Matrix<T> Matrix<T>::operator*(const Matrix<T>& other){
	 if(this->cols() != other.rows()){
	    std::cerr << "Dimensions dont fit in operator*" << std::endl;
	    std::cout << "   " << this->rows() << "," << this->cols() << "   " << other.rows() << "," << other.cols() << std::endl;
	 }
	 
	 Matrix<T> res(this->rows(), other.cols());

	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < other.cols();j++){
	       T val = 0;
	       for(int k = 0; k < this->cols(); k++){
		  val += this->operator()(i,k)*other(k,j);
	       }
	       res.set(i,j,val);
	    }
	 }
	 return res;
      }
      
      template<class T> Matrix<T> Matrix<T>::operator*(const T& scalar){	 
	 Matrix<T> res(this->rows(), this->cols());

	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < this->cols();j++){
	       T val = this->operator()(i,j) * scalar;
	       res.set(i,j,val);
	    }
	 }
	 return res;
      }
      
      template<class T> T& Matrix<T>::operator()(const int row, const int col) const {
	 return _data[row][col];
      }
      
      template<class T> Matrix<T> Matrix<T>::transpose(){
	 Matrix<T> res(this->cols(), this->rows());

	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < this->cols();j++){
	       res.set(j,i,this->operator()(i,j));
	    }
	 }
	 return res;
      }
      
      template<class T> Matrix<T> Matrix<T>::identity(){
	 if(this->rows() != this->cols()){
	    std::cout << "Cannot make a non-square matrix identity" << std::endl;
	    return *this;
	 }
	 for(int i = 0; i < _rows; i++){
	    this->set(i,i,1);
	 }
	 return *this;
      }
      
      template<class T> Matrix<T> Matrix<T>::replicate(int cols){
	 Matrix<T> res(cols, this->cols());//, cols);
	 
	 for(int k = 0; k < cols; k++){
	    for(int i = 0; i < this->rows(); i++){
	       for(int j = 0; j < this->cols(); j++){
		  res.set(k,j,this->operator()(i,j));
	       }
	    }
	 }
	 return res;
      }
      
      template<class T> Matrix<T> Matrix<T>::unary_expr(std::function<T(T)> func){	 
	 for(int i = 0; i < this->rows(); i++){
	    for(int j = 0; j < this->cols(); j++){
	       this->set(i,j,func(this->operator()(i,j)));
	    }
	 }
	 return *this;
      }
      
      template<class T> void Matrix<T>::set(int row,int col,T value){
	 _data[row][col] = value;
      }
      
      template<class T> void Matrix<T>::print(){
	 for(int i = 0; i < _rows; i++){
	    for(int j = 0; j < _cols; j++){
	       printf("%.6f   ", (float) _data[i][j]);
	    }
	    printf("\n");
	 }
	 printf("\n");
      }*/
   }
}

#endif // MATRIX_HPP
