/*
 * Copyright 2016 Nicolai Anton Lynnerup <nlynn12@student.sdu.dk>
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

#ifndef _CUDANN_MATH_MATRIX_HOST_HPP
#define _CUDANN_MATH_MATRIX_HOST_HPP

#include <iostream>
#include <cstdio>
#include <functional>

#include "Matrix.hpp"

namespace cudann {
   namespace math {
      template <typename T>
      class MatrixHost{
	 private:
	 public:
	    //Matrix<T>& operator=(const Matrix<T>& other);
	    void sum();
      };
      
      /*template <typename T> Matrix<T>& MatrixHost::operator=(const Matrix<T>& other){
	 if(this->cols() != other.cols() || this->rows() != other.rows()){
	    std::cerr << "Dimensions dont fit in operator=" << std::endl;
	    std::cout << "   " << this->rows() << "," << this->cols() << "   " << other.rows() << "," << other.cols() << std::endl;
	 }
	 for(size_t i = 0; i < this->rows(); i++){
	    for(size_t j = 0; j < this->cols(); j++){
	       this->_data[i][j] = other(i,j);
	    }
	 }
	 return *this;
      }*/
      template <typename T> void MatrixHost::sum(){
	 std::cout << "Sum" << std::endl;
      }
   }
}

#endif // MATRIX_HPP
