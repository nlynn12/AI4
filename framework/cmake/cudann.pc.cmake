prefix=@CMAKE_INSTALL_PREFIX@
exec_prefix=@BIN_INSTALL_DIR@
libdir=@LIB_INSTALL_DIR@
includedir=@INCLUDE_INSTALL_DIR@
 
Name: CUDANN
Description: CUDA ANN Framework
Version: @VERSION@
Libs: -L${libdir} -lm -lcudann
Cflags: -I${includedir}
