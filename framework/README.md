# About CUDANN
CUDANN is a framework developed using CUDA, C++11, and Boost.Python which enables a seamless interoperability between the framework and the Python programming language. The framework is easy to install and integrates nicely with your existing C++ and Python projects. The framework supports a range of different artificial neural networks and training techniques.

# Installing CUDANN
To install CUDANN simply follow these steps:
- Make sure that you have all the dependencies installed:
   - CMake 2.8.8 or higher
   - CUDA 7.0 or higher (and of course a compatible GPU)
   - Boost
- Next download the project and unzip it to your favorite location
- Create a build directory inside the project and build it

```shell
mkdir build
cd build/
cmake ../
sudo make install
```

- This will install both the static and shared libraries on your platform
