#include <iostream>
#include <cudann/cudann.hpp>

int main ( int argc, char *argv[] ) {
    for(int i = 1000000; i > 0; i--) std::cout << run(i) << std::endl;
    return 0;
}
