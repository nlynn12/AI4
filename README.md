# An Investigational Work in the field of AI

This project concerns partly an investigation of existing frameworks for training deep architectures and partly an investigation of the state of the art methods used in classifying the [MNIST dataset](http://yann.lecun.com/exdb/mnist/). Furthermore it concerns the development of a new framework in order to accept a wider range of input data.

The project is developed by: Nicolai Anton Lynnerup and Jens-Jakob Bentsen as a master course at University of Southern Denmark. As the framework and investigational work is performed by academics it is of most importance to us that you cite our work when used ([BibTeX]()).

# Structure of Project.


# Wikis
* [Connecting to Marvin-2-TEK](https://gitlab.com/nlynn12/AI4/wikis/marvin)
* [PCI-e](https://gitlab.com/nlynn12/AI4/wikis/pci-e)
